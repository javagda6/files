package com.amen.sda.files.zadanie1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainZadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        try(PrintWriter writer = new PrintWriter("file.txt")){
        try (PrintWriter writer = new PrintWriter(new FileWriter("plikForm.txt", true))) {
            while(scanner.hasNextLine()) {
                System.out.println("Imie i nazwisko:");
                String imie = scanner.nextLine();
            if(imie.equals("quit")){
                break;
            }

                System.out.println("Wiek: ");
                String wiek = scanner.nextLine();

                System.out.println("Plec [kobieta/mezczyzna]:");
                String plec = scanner.nextLine();
                int wiekInt = Integer.parseInt(wiek);

                writer.println(imie); // odp1
                writer.println(wiek); // odp2
                writer.println(plec); // odp3
                if (plec.equals("mezczyzna") && wiekInt > 25 && wiekInt < 30) {
                    // kolejne pytanie.
                    String dodatkowa = scanner.nextLine();
                    writer.println(dodatkowa);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
