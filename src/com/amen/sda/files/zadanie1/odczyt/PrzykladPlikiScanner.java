package com.amen.sda.files.zadanie1.odczyt;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Logger;

public class PrzykladPlikiScanner {
    public static void main(String[] args) {

        File plik = new File("plik.txt");

        if (plik.exists()) {
            System.out.println("Plik istnieje");
        } else {
            System.out.println("Plik nie istnieje");
        }

        if (plik.isDirectory()) {
            System.out.println("Jest katalogiem");
        }

        // Scanner
//        metodaCzytania(plik);
        try(Scanner scanner = new Scanner(plik)){


            String linia = null;
            while(scanner.hasNextLine()){
                linia = scanner.nextLine();

                System.out.println("Linia: " + linia);
            }

        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        }
        //        Scanner scanner = new Scanner();
    }

//    private static String metodaCzytania(File plik) {
//        try(Scanner scanner = new Scanner(plik)){
//
//
//            String linia = null;
//            while(scanner.hasNextLine()){
//                linia = scanner.nextLine();
//
//                return linia;
////                System.out.println("Linia: " + linia);
//            }
//
//        } catch (FileNotFoundException fnfe) {
//            System.out.println("Nie ma takiego pliku.");
//        }finally {
//
//        }
//
//        return null;
//    }
}
