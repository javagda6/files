package com.amen.sda.files.zadanie1.odczyt;

import java.io.*;
import java.util.Scanner;

public class PrzykladPliki {
    public static void main(String[] args) {
        File plik = new File("C:\\plik.txt");

        if (plik.exists()) {
            System.out.println("Plik istnieje");
        } else {
            System.out.println("Plik nie istnieje");
        }

        if (plik.isDirectory()) {
            System.out.println("Jest katalogiem");
        }

        // BufferedReader
        // Scanner
        try (FileReader readerF = new FileReader("plik.txt");
             BufferedReader reader = new BufferedReader(readerF)) {
//            FileReader fileReader = ;

            String linia = null;
            do {
                linia = reader.readLine();

                System.out.println("Linia: " + linia);
            } while (linia != null);

        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
